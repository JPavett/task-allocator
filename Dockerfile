FROM php:8.1.1-fpm as base

COPY docker/php.ini /usr/local/etc/php/php.ini

# Install App Dependencies
RUN mkdir -p /usr/share/man/man1 && apt-get --allow-releaseinfo-change update && \
    apt-get install -y -q --no-install-recommends \
    nano apt-utils curl unzip default-mysql-client build-essential git \
    libcurl4-gnutls-dev libmcrypt-dev libmagickwand-dev \
    libwebp-dev libjpeg-dev libpng-dev libxpm-dev \
    libfreetype6-dev libaio-dev zlib1g-dev libzip-dev pdftk openssh-client && \
    echo 'umask 002' >> /root/.bashrc


# Docker PHP Extensions
RUN docker-php-ext-install -j$(nproc) iconv gettext gd mysqli curl pdo pdo_mysql zip pcntl && \
    docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ && \
    docker-php-ext-configure curl --with-curl && \
    docker-php-ext-configure pcntl --enable-pcntl

# Cleaning
RUN apt-get clean && apt-get autoremove -y

# Install and configure Node
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash
RUN apt-get -y install nodejs

# Production Environment Dependencies
FROM base as prod_dependencies

COPY ./webroot /var/www/html

ENV TELESCOPE_ENABLED=false

COPY --from=composer:2.1 /usr/bin/composer /usr/local/bin/composer
ENV PATH /var/www/html/vendor/bin:/root/.composer/vendor/bin:$PATH
RUN composer install --no-progress

RUN touch /var/www/html/public/mix-manifest.json
RUN chown -R www-data:www-data /var/www/html/public
RUN chmod -R 755 /var/www/html/public
RUN npm install --no-progress
RUN npm run prod --silent --no-progress

# Development Environment Dependencies
FROM base as build_dev

COPY ./webroot /var/www/html

RUN pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.client_port=9009" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.log=/var/log/xdebug.log" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.log_level=0" >> /usr/local/etc/php/conf.d/xdebug.ini && \
    echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/xdebug.ini

ENV PHP_IDE_CONFIG 'serverName=DockerApp'

COPY --from=composer:2.1 /usr/bin/composer /usr/local/bin/composer
ENV PATH /var/www/html/vendor/bin:/root/.composer/vendor/bin:$PATH

RUN chown -R www-data:www-data /var/www/
RUN chmod -R 755 /var/www/html/storage/

EXPOSE 9000
EXPOSE 9009

# Build for Production
FROM base as build_prod

COPY docker/id_rsa /root/.ssh/id_rsa

RUN chmod 400 /root/.ssh/id_rsa
RUN echo "    StrictHostKeyChecking no      " >> /etc/ssh/ssh_config

COPY ./webroot /var/www/html
COPY --from=prod_dependencies /var/www/html/vendor/ /var/www/html/vendor/
COPY --from=prod_dependencies /var/www/html/public/js/ /var/www/html/public/js/
COPY --from=prod_dependencies /var/www/html/public/css/ /var/www/html/public/css/
COPY --from=prod_dependencies /var/www/html/public/mix-manifest.json /var/www/html/public/mix-manifest.json

RUN chown -R www-data:www-data /var/www/
RUN chmod -R 755 /var/www/html/storage/

EXPOSE 9000