<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\Team;
use Illuminate\Http\JsonResponse;

class TeamController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Team::class, 'team');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    : JsonResponse
    {
        return $this->sendResponse(Team::all()->toArray(), 'Teams Retrieved Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTeamRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTeamRequest $request)
    : JsonResponse {
        $validated = $request->validated();

        $team              = new Team;
        $team->name        = $validated['name'];
        $team->description = $validated['description'];
        $team->department  = $validated['department'];
        $team->save();
        $team->load(['tasks', 'users']);

        return $this->sendResponse($team->toArray(), 'Team Created Successfully', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Team  $team
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Team $team)
    : JsonResponse {
        return $this->sendResponse($team->load(['tasks', 'users'])->toArray(), 'Team Retrieved Successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTeamRequest  $request
     * @param  \App\Models\Team  $team
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTeamRequest $request, Team $team)
    : JsonResponse {
        $validated = $request->validated();

        $team->name        = $validated['name'];
        $team->description = $validated['description'];
        $team->department  = $validated['department'];
        $team->save();

        return $this->sendResponse($team->toArray(), 'Team Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Team  $team
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Team $team)
    : JsonResponse {
        $team->delete();

        return $this->sendResponse($team->toArray(), 'Team Deleted Successfully', 204);
    }
}
