<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Successful JSON Response
     *
     * @param  mixed  $result
     * @param  string  $message
     * @param  int  $code
     *
     * @return JsonResponse
     */
    public function sendResponse(mixed $result, string $message, int $code = 200)
    : JsonResponse {

        $response = [
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, $code);
    }
}
