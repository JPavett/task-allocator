<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Models\Task;
use Illuminate\Http\JsonResponse;

class TaskController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(Task::class, 'task');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    : JsonResponse
    {
        return $this->sendResponse(Task::with(['team', 'users'])->get()->toArray(), 'Tasks Retrieved Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTaskRequest  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTaskRequest $request)
    : JsonResponse {
        $validated = $request->validated();

        $task              = new Task;
        $task->title       = $validated['title'];
        $task->description = $validated['description'];
        $task->priority    = $validated['priority'];
        $task->team_id     = $validated['team_id'];
        $task->save();
        $task->load(['team', 'users']);

        return $this->sendResponse($task->toArray(), 'Task Created Successfully', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Task $task)
    : JsonResponse {
        return $this->sendResponse($task->load(['team', 'users'])->toArray(), 'Task Retrieved Successfully');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTaskRequest  $request
     * @param  \App\Models\Task  $task
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTaskRequest $request, Task $task)
    : JsonResponse {
        $validated = $request->validated();

        $task->title       = $validated['title'];
        $task->description = $validated['description'];
        $task->priority    = $validated['priority'];
        $task->save();

        if(array_key_exists('user_ids', $validated) && is_array($validated['user_ids'])){
            $task->users()->sync($validated['user_ids']);
        }

        $task->load(['team', 'users']);

        return $this->sendResponse($task->toArray(), 'Task Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Task $task)
    : JsonResponse {
        $task->delete();

        return $this->sendResponse($task->toArray(), 'Task Deleted Successfully', 204);
    }
}
