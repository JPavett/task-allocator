<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    /**
     * Log into the application
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logIn(Request $request)
    : JsonResponse {
        $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if (Auth::guard()->attempt($request->only('email', 'password'))) {
            $request->session()->regenerate();

            /** @var User $user */
            $user = Auth::user();

            return $this->sendResponse($user->load(['team'])->toArray(), 'Log In Successful');
        }

        return $this->sendResponse([], 'Cannot Log In, Invalid Credentials', 401);
    }

    /**
     * Log out of the application
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logOut(Request $request)
    : JsonResponse {
        Auth::guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $this->sendResponse([], 'Log Out Successful', 204);
    }

    /**
     * Log out of the application
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    : JsonResponse {
        $validated = $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|unique:users',
            'password' => 'required|confirmed',
        ]);

        $user                    = new User;
        $user->name              = $validated['name'];
        $user->email             = $validated['email'];
        $user->email_verified_at = now();
        $user->password          = Hash::make($validated['password']);
        $user->super_admin       = 0;
        $user->save();

        return $this->sendResponse($user->toArray(), 'Registration Successful', 201);
    }
}
