<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create the controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    : JsonResponse {
        return $this->sendResponse(User::with(['team', 'tasks'])->get()->toArray(), 'Users Retrieved Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    : JsonResponse {
        return $this->sendResponse($user->toArray(), 'User Retrieved Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    : JsonResponse {
        $validated = $request->validated();

        $user->name  = $validated['name'];
        $user->email = $validated['email'];

        // Set Team ID if supplied
        if (array_key_exists('team_id', $validated)) {
            $user->team_id = $validated['team_id'];
        }

        // Set Password if supplied
        if (array_key_exists('password', $validated) && ! is_null($validated['password'])) {
            $user->password = Hash::make($validated['password']);
        }

        $user->save();
        $user->load(['team', 'tasks']);

        return $this->sendResponse($user->toArray(), 'User Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    : JsonResponse {
        $user->delete();

        return $this->sendResponse($user->toArray(), 'User Deleted Successfully', 204);
    }
}
