<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['title' => "string", 'description' => "string", 'priority' => "string", 'user_ids' => "string"])]
    public function rules()
    : array
    {
        return [
            'title'       => 'required|string',
            'description' => 'required|string',
            'priority'    => 'required|numeric',
            'user_ids'    => 'nullable',
        ];
    }
}
