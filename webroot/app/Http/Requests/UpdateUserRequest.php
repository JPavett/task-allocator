<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    : bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['name' => "string", 'email' => "string", 'team_id' => "string", 'password' => "string"])]
    public function rules()
    : array
    {
        /** @var User $user */
        $user = $this->user;

        return [
            'name'    => 'required|string',
            'email'   => 'required|string|unique:users,email,' . $user->id.',id',
            'team_id' => 'nullable|integer',
            'password' => 'nullable|confirmed'
        ];
    }
}
