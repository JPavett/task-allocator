<?php

namespace App\Policies;

use App\Models\Task;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use JetBrains\PhpStorm\Pure;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function viewAny(User $user)
    : Response|bool {
        return $user->isSuperAdmin();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Task  $task
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function view(User $user, Task $task)
    : Response|bool {
        return $user->isSuperAdmin() || ($user->team_id === $task->team_id);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Auth\Access\Response|bool
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function create(User $user)
    : Response|bool {
        return $user->isSuperAdmin() || ($user->team_id == request()->get('team_id'));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Task  $task
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function update(User $user, Task $task)
    : Response|bool {
        return $user->isSuperAdmin() || ($user->team_id === $task->team_id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function delete(User $user)
    : Response|bool {
        return $user->isSuperAdmin();
    }
}
