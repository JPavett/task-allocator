<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use JetBrains\PhpStorm\Pure;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function viewAny(User $user)
    : Response|bool {
        return $user->isSuperAdmin();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $requestUser
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function view(User $user, User $requestUser)
    : Response|bool {
        return $user->isSuperAdmin() || ($requestUser->id === $user->id);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $requestUser
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function update(User $user, User $requestUser)
    : Response|bool {
        return $user->isSuperAdmin() || ($requestUser->id === $user->id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $requestUser
     *
     * @return \Illuminate\Auth\Access\Response|bool
     */
    #[Pure]
    public function delete(User $user, User $requestUser)
    : Response|bool {
        return $user->isSuperAdmin() || ($requestUser->id === $user->id);
    }
}
