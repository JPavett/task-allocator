@servers(['production' => 'ssh.jamespavett.dev', 'staging' => 'ssh.staging.jamespavett.dev'])

@setup
$currentDir = '/var/task-allocator';
$containerDir = '/var/www/html';

function logMessage($message, $single = false) {
    if($single == false){
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }
}
@endsetup

@story('deploy', ['on' => ['production']])
    git
    container
    cache
@endstory

@story('deploy-staging', ['on' => ['staging']])
    git-staging
    container-staging
    cache-staging
@endstory

@task('container', ['on' => ['production']])
    {{ logMessage("Updating Docker container on Production Server")}}
    cd {{ $currentDir }}

    docker compose -f compose.prod.yaml pull
    docker compose -f compose.prod.yaml up --build -d --remove-orphans --force-recreate
    docker compose -f compose.prod.yaml down
    docker volume rm task-allocator_container-data
    docker compose -f compose.prod.yaml up -d
@endtask

@task('container-staging', ['on' => ['staging']])
    {{ logMessage("Updating Docker Container on Production Server")}}
    cd {{ $currentDir }}

    docker compose -f compose.prod.yaml pull
    docker compose -f compose.prod.yaml up --build -d --remove-orphans --force-recreate
    docker compose -f compose.prod.yaml down
    docker volume rm task-allocator_container-data
    docker compose -f compose.prod.yaml up -d
@endtask

@task('git', ['on' => ['production']])
    {{ logMessage("Updating Git Repository on Production Server")}}
    cd {{ $currentDir }}

    git fetch --prune
    git pull
@endtask

@task('git-staging', ['on' => ['staging']])
    {{ logMessage("Updating Git Repository on Test Server")}}
    cd {{ $currentDir }}

    git fetch --prune
    git pull
@endtask

@task('down', ['on' => ['production']])
@if ($down)
    {{ logMessage("Putting Node into Maintenance Mode") }}

    cd {{ $currentDir }}

    docker compose exec -T app bash

    php {{ $containerDir }}/artisan down
@else
    {{ logMessage("Not Putting Node into Maintenance Mode") }}
@endif
@endtask

@task('down-staging', ['on' => ['staging']])
@if ($down)
    {{ logMessage("Putting Test Server into Maintenance Mode") }}

    cd {{ $currentDir }}

    docker compose exec -T app bash

    php {{ $containerDir }}/artisan down
@else
    {{ logMessage("Not Putting Test Server into Maintenance Mode") }}
@endif
@endtask

@task('up', ['on' => ['production']])
    @if ($up)
        {{ logMessage("Taking Node out of Maintenance Mode") }}

        cd {{ $currentDir }}

        docker compose exec -T app bash

        php {{ $containerDir }}/artisan up
    @else
        {{ logMessage("Leaving Node in Maintenance Mode") }}
    @endif
@endtask

@task('up-staging', ['on' => ['staging']])
    @if ($up)
        {{ logMessage("Taking Test Server out of Maintenance Mode") }}

        cd {{ $currentDir }}

        docker compose exec -T app bash

        php {{ $containerDir }}/artisan up
    @else
        {{ logMessage("Leaving Test Server in Maintenance Mode") }}
    @endif
@endtask

@task('cache', ['on' => ['production']])
    {{ logMessage("Clearing and building new cache on Production Server")}}

    cd {{ $currentDir }}

    docker compose exec -T app bash

    php {{ $containerDir }}/artisan optimize

    php {{ $containerDir }}/artisan event:cache

    php {{ $containerDir }}/artisan storage:link

    chmod -R 755 {{ $containerDir }}/storage/
@endtask

@task('cache-staging', ['on' => ['staging']])
    {{ logMessage("Clearing and building new cache on Staging Server")}}

    cd {{ $currentDir }}

    docker compose exec -T app bash

    php {{ $containerDir }}/artisan optimize

    php {{ $containerDir }}/artisan event:cache

    php {{ $containerDir }}/artisan storage:link

    chmod -R 755 {{ $containerDir }}/storage/
@endtask

@finished
    if (!$exitCode) {
        echo "\033[32m" . "All Tasks Complete" . "\033[0m" . "\n";
    }
@endfinished
