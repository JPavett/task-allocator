<?php

namespace Database\Seeders;

use App\Models\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $task              = new Task;
        $task->title       = 'Write-up minutes from WMS Pick Enhancement meeting 04/01/2022';
        $task->description = 'Following the WMS enhancement meeting on 04/01/2022, all relevant notes and actions need to be written up and circulated to stakeholders';
        $task->priority    = 3;
        $task->team_id     = 1;
        $task->save();

        $task              = new Task;
        $task->title       = 'Complete UAT Testing for V507';
        $task->description = 'Testing needs to be completed, with any issues or defects raised to DAI and chased for a resolution.';
        $task->priority    = 2;
        $task->team_id     = 1;
        $task->save();
    }
}
