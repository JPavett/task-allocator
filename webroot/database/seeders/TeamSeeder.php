<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $team = new Team;
        $team->name = 'Application Support';
        $team->description = 'The retail IT Application Support team';
        $team->department = 'IT';
        $team->save();

        $team = new Team;
        $team->name = 'Desktop Support';
        $team->description = 'The retail IT Desktop Support team';
        $team->department = 'IT';
        $team->save();

        $team = new Team;
        $team->name = 'Infrastructure Support';
        $team->description = 'The retail IT Application Support team';
        $team->department = 'IT';
        $team->save();

        $team = new Team;
        $team->name = 'Projects';
        $team->description = 'The Project Management team';
        $team->department = 'Development';
        $team->save();
    }
}
