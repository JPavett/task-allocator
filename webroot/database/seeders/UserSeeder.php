<?php

namespace Database\Seeders;

use App\Models\User;
use Config;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seed Admin User
        $user                    = new User;
        $user->name              = 'Admin Account';
        $user->email             = 'admin@account.co.uk';
        $user->email_verified_at = now();
        $user->password          = Hash::make(Config::get('app.default_password'));
        $user->super_admin       = true;
        $user->team_id           = 1;
        $user->save();

        // Seed Basic User
        $user                    = new User;
        $user->name              = 'Basic Account';
        $user->email             = 'basic@account.co.uk';
        $user->email_verified_at = now();
        $user->password          = Hash::make(Config::get('app.default_password'));
        $user->super_admin       = false;
        $user->team_id           = 1;
        $user->save();

        // Seed Generic Users
        User::factory(20)->create();
    }
}
