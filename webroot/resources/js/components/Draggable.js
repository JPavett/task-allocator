import React from 'react'
import {useDrag} from 'react-dnd'

const Draggable = ({isDragging = false, itemData, children, fullHeight = true}) => {

    const fillHeight = () => {
        if(fullHeight){
            return '100%'
        } else {
            return 'auto'
        }
    }

    const [{opacity}, dragRef] = useDrag(
        () => ({
            type: 'card',
            item: itemData,
            collect: (monitor) => ({
                opacity: monitor.isDragging() ? 0.5 : 1
            })
        }), [itemData]
    )

    return (
        <div ref={dragRef} style={{opacity, height: fillHeight()}}>
            {children}
        </div>
    )
}

export default Draggable
