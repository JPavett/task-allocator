import React from 'react'
import useAuth from "../utilities/useAuth";
import {Navigate} from "react-router-dom";

export default function RequireAuthentication({children}) {
    const {authed} = useAuth();

    // If User is authenticated, shown children elements, else navigate to login page
    return authed === true
        ? children
        : <Navigate to="/login" replace/>;
}
