import React from 'react'
import {Link} from 'react-router-dom'
import useAuth from "../utilities/useAuth";
import {useNavigate} from "react-router";
import User from "../utilities/user";

const AuthenticationIndicator = () => {

    const {logout} = useAuth();
    const navigate = useNavigate();

    // If user is logged in return authentication information
    if (User?.isLoggedIn) {
        return (
            <div>
                <span className="px-3 py-2 rounded-md text-sm font-medium text-black">Hi, {User.name}</span>
                <span className="px-3 py-2 rounded-md text-sm font-medium text-black cursor-pointer hover:text-white hover:bg-rose-500 focus:outline-none "
                      onClick={() => {
                          navigate("/users/" + User.id);
                      }}>
                    Account Details
                </span>
                <span className="px-3 py-2 rounded-md text-sm font-medium text-black cursor-pointer hover:text-white hover:bg-rose-500 focus:outline-none "
                      onClick={() => {
                          logout().then(() => {
                              User.loggedOut()
                              navigate("/login");
                          });
                      }}>
                    Sign Out
                </span>
            </div>
        )
    }

    // Otherwise, show button for sign in page
    return (
        <Link to="/login">
            <span className="px-3 py-2 rounded-md text-sm font-medium text-indigo-300 hover:text-white hover:bg-indigo-700 focus:outline-none focus:text-white focus:bg-indigo-700">
                Sign In
            </span>
        </Link>
    )
}

export default AuthenticationIndicator
