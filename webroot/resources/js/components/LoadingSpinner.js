import React from 'react'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const LoadingSpinner = ({loading, children}) => {
    // Either return loading spinner or children elements
    if (loading === true) {
        return (
            <div className="mx-auto mt-12 text-center">
                <FontAwesomeIcon className="fa-spin fa-2x" icon={"spinner"}/>
            </div>
        )
    } else {
        return <div>{children}</div>
    }
}

export default LoadingSpinner
