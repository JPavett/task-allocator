import React from 'react'
import Badge from "./Badge";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import User from "../utilities/user";

const Card = ({
                  title,
                  subTitle,
                  description,
                  selected,
                  cardClass = '',
                  editable = false,
                  draggable = false,
                  allowEditByRegularUsers = false,
                  priority,
                  timestamps,
                  onClick,
                  onEdit,
              }) => {

    // Show editable icon based on props, or if User is Super Admin
    const editableIcon = () => {
        if (editable && (User.isSuperAdmin() || allowEditByRegularUsers)) {
            return <FontAwesomeIcon onClick={onEdit}
                                    title="Edit"
                                    style={{cursor: 'pointer'}}
                                    className="mt-1 mr-2"
                                    icon={'edit'}/>
        }
    }

    // Show draggable icon based on prop
    const draggableIcon = () => {
        if (draggable) {
            return <FontAwesomeIcon title="Can be dragged" className="mt-1" icon={'arrows-alt'}/>
        }
    }

    // Show selected badge based on prop
    const selectedBadge = () => {
        if (selected) {
            return (
                <div className="mr-2">
                    <Badge text="Selected"/>
                </div>
            )
        }
    }

    return (
        <div style={{cursor: draggable ? 'move' : 'auto', height: '100%'}}
             className={'relative bg-white shadow rounded border m-2 flex  ' + cardClass}
             onClick={onClick}>
            <div className="px-4 py-2 w-full flex flex-col">
                <div className="flex">
                    <span className="grow text-lg font-bold">
                        {title}
                    </span>
                    {selectedBadge()}
                    {editableIcon()}
                    {draggableIcon()}
                </div>
                <p className="block mb-2 text-sm text-gray-600">{subTitle}</p>
                <p className="grow">
                    {description}
                </p>
                <div className="justify-self-end">
                    {priority} {timestamps}
                </div>
            </div>
        </div>
    )
}

export default Card
