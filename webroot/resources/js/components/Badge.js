import React from 'react'

const Badge = ({text}) => {
    return (
        <span style={{marginTop: '2px'}}
              className="h-4 mt-2 inline-flex items-center justify-center px-2 text-xs font-bold leading-none text-white bg-green-500 rounded uppercase">{text}</span>
    )
}

export default Badge
