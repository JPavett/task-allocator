import React from 'react'
import {useDrop} from 'react-dnd'

const Droppable = ({dropData, children, onDrop, fullHeight = true}) => {

    const fillHeight = () => {
        if(fullHeight){
            return '100%'
        } else {
            return 'auto'
        }
    }

    const [collectedProps, drop] = useDrop(() => ({
        accept: 'card',
        drop: (dragData) => {
            onDrop(dropData, dragData)
        }
    }), [dropData])

    return (
        <div ref={drop} style={{height: fillHeight()}}>
            {children}
        </div>
    )
}

export default Droppable
