import React from 'react'
import PriorityIndicator from "./PriorityIndicator";
import Card from "./Card";
import moment from "moment";
import {useNavigate} from "react-router";

const TaskCard = ({task}) => {

    const navigate = useNavigate();

    const taskTimeStamps = (task) => {

        // Formatted created at time for Task
        const createdAt = () => {
            return (
                <p className="block text-sm mt-2 text-gray-600"> Created
                    at: {moment(task?.created_at).format('DD/MM/YYYY HH:mm:ss')}</p>
            )

        }

        // Formatted updated at time for Task
        const updatedAt = () => {
            return (
                <p className="block mb-2 text-sm text-gray-600">Updated
                    at: {moment(task?.updated_at).format('DD/MM/YYYY HH:mm:ss')}</p>
            )

        }

        return (
            <div>
                {createdAt()}
                {updatedAt()}
            </div>
        )
    }

    // Get list of users names assigned to task
    const getUsersAssignedToTask = task => {
        return task.users.map(user => user.name)
    }

    // Convert usernames into friendly string
    const usersAssignedToTaskString = (task) => {
        const users = getUsersAssignedToTask(task)

        if (users.length === 0) return 'Unallocated';
        if (users.length === 1) return users[0];

        const last = users.pop();
        return users.join(', ') + ' and ' + last;
    }

    const editTask = (task) => {
        navigate('/tasks/' + task.id)
    }

    return (
        <Card title={task.id + ') ' + task.title}
              subTitle={'Assigned to: ' + usersAssignedToTaskString(task)}
              priority={<PriorityIndicator number={task.priority}/>}
              timestamps={taskTimeStamps(task)}
              description={task.description}
              editable={true}
              allowEditByRegularUsers={true}
              onEdit={() => editTask(task)}/>)
}

export default TaskCard
