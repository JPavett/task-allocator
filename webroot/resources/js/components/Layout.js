import React from 'react'
import AuthenticationIndicator from './AuthenticationIndicator'
import {NavLink} from "react-router-dom";
import User from "../utilities/user";

const Layout = ({title = '', children}) => {

    // Only show Teams Tab for Super Admins
    const showTeamsTab = () => {
        if(User.isSuperAdmin()){
            return <NavLink to="/teams" className={navData =>
                "px-3 py-2 rounded-md text-sm font-medium focus:outline-none hover:bg-rose-500 hover:text-white" + (navData.isActive ? " bg-rose-500 text-white" : " text-black")}>
                Teams
            </NavLink>
        }
    }

    // noinspection HtmlUnknownTarget
    return (
        <div>
            <nav className="bg-gray-100">
                <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                    <div className="flex items-center justify-between h-14">
                        <div className="flex items-center">
                            <div className="flex-shrink-1">
                                <img style={{width: 50}} src="/storage/images/logo.svg" alt="Workflow logo"/>
                            </div>
                            <div className="hidden md:block">
                                <div className="ml-10 flex items-baseline space-x-4">
                                    <NavLink to="/" className={navData =>
                                        "px-3 py-2 rounded-md text-sm font-medium focus:outline-none hover:bg-rose-500 hover:text-white" + (navData.isActive ? " bg-rose-500 text-white" : " text-black")}>
                                        My Tasks
                                    </NavLink>
                                    <NavLink to={'/tasks/team/' + (User?.team?.id ?? '')} className={navData =>
                                        "px-3 py-2 rounded-md text-sm font-medium focus:outline-none hover:bg-rose-500 hover:text-white" + (navData.isActive ? " bg-rose-500 text-white" : " text-black")}>
                                        Team Tasks
                                    </NavLink>
                                    {showTeamsTab()}
                                </div>
                            </div>
                        </div>
                        <div>
                            <AuthenticationIndicator/>
                        </div>
                    </div>
                </div>
            </nav>

            <header className="bg-white shadow">
                <div className="max-w-7xl mx-auto py-3 px-4 sm:px-6 lg:px-8">
                    <h1 className="text-2xl font-bold leading-tight">
                        {title}
                    </h1>
                </div>
            </header>

            <main>
                <div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
                    {children}
                </div>
            </main>
        </div>
    )
}

export default Layout
