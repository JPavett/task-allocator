import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const PriorityIndicator = ({number}) => {

    // Toggle indicator colour via supplied prop
    let priorityClass = ''
    switch(number) {
        case 1:
            priorityClass = 'text-green-500'
            break;
        case 2:
            priorityClass = 'text-yellow-300'
            break;
        case 3:
            priorityClass = 'text-amber-500'
            break;
        case 4:
            priorityClass = 'text-orange-600'
            break;
        case 5:
            priorityClass = 'text-red-600'
            break;
        default:
    }

    return (
        <div title={'Priority: ' + number}>
            {[...Array(number).keys()].map((number, index) => (
                <FontAwesomeIcon key={index} className={'mr-1 ' + priorityClass} icon={'circle'}/>
            ))}
        </div>
    )
}

export default PriorityIndicator
