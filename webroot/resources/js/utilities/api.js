import axios from 'axios'
import toast from "react-hot-toast";
import User from './user'

export default function api() {
    const api = axios.create({
        baseURL: '/',
        withCredentials: true
    })

    api.interceptors.response.use(response => response, async error => {

        if (error.response.status !== 401 && error.response.status !== 403) {
            // Toast object or array of error messages
            if (error.response?.data?.errors) {
                for (const key in (error.response?.data?.errors)) {
                    toast.error(error.response?.data?.errors[key]?.[0]);
                }
            } else {
                // Toast single error messages
                if (error.response?.data?.error || error.response?.data?.message) {
                    toast.error(error.response?.data?.error || error.response?.data?.message);
                }
            }
        }

        if (error.response.status === 401) {
            // Either toast for invalid credentials, or redirect user to login page
            if(error.response?.data?.message === 'Cannot Log In, Invalid Credentials'){
                toast.error(error.response.data.message);
            } else {
                toast.error('Request Unauthorized, Redirected to Log In Page');

                return setTimeout(() => {
                    User.loggedOut();
                    window.location = '/login'
                    return Promise.reject()
                }, 2000);
            }
        }

        // Navigate home if user does not have permission to access resource
        if (error.response.status === 403) {
            toast.error('Request Forbidden, You do not have the correct permissions');
            return setTimeout(() => {
                window.location = '/'
                return Promise.reject()
            }, 2000)
        }

        return Promise.reject(error)
    })

    return api
}
