import * as React from "react";
import User from "./user";

const authContext = React.createContext(undefined, undefined);

function useAuth() {
    const [authed, setAuthed] = React.useState(User.isLoggedIn());

    return {
        authed,
        login() {
            return new Promise((res) => {
                if (User.isLoggedIn()) {
                    setAuthed(true);
                    res();
                }
            });
        },
        logout() {
            return new Promise((res) => {
                setAuthed(false);
                res();
            });
        }
    };
}

export function AuthProvider({children}) {
    const auth = useAuth();

    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
}

export default function AuthConsumer() {
    return React.useContext(authContext);
}
