class User {

    constructor() {
        this.init()
    }

    /**
     * Initialise User
     */
    init() {
        let user = JSON.parse(localStorage.getItem('user'))

        this.id = user?.id || ''
        this.name = user?.name || ''
        this.email = user?.email || ''
        this.loggedIn = !!user?.id
        this.superAdmin = user?.super_admin === 1 ? true : false
        this.team = user?.team || {}
    }

    /**
     * Set user data in local storage and initialise User
     *
     * @param user object
     * @param user.id number
     * @param user.name string
     * @param user.email string
     */
    authenticated(user) {
        localStorage.setItem('user', JSON.stringify(user))

        this.init()
    }

    /**
     * Log user our and reinitialise User
     */
    loggedOut() {
        localStorage.removeItem('user')

        this.init()
    }

    /**
     * Check if user is logged in
     *
     * @return {boolean}
     */
    isLoggedIn() {
        return Boolean(this.loggedIn) === true
    }

    /**
     * Check if user is Super Admin
     *
     * @return {boolean}
     */
    isSuperAdmin() {
        return this.superAdmin
    }
}

export default new User()
