require('./bootstrap');

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {AuthProvider} from "./utilities/useAuth";
import {Toaster} from 'react-hot-toast';
import {library} from '@fortawesome/fontawesome-svg-core';
import {
    faArrowsAlt,
    faCircle,
    faEdit,
    faLock,
    faPlus,
    faSpinner,
    faTimes,
    faTrash,
    faUser
} from '@fortawesome/free-solid-svg-icons';
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import Home from "./App";

library.add(faUser, faLock, faSpinner, faArrowsAlt, faEdit, faTrash, faPlus, faCircle, faTimes);

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <DndProvider backend={HTML5Backend}>
                <AuthProvider>
                    <Home/>
                </AuthProvider>
            </DndProvider>
        </BrowserRouter>
        <Toaster reverseOrder={true} position="bottom-center" gutter={8} toastOptions={{
            style: {
                minWidth: '300px',
            },
        }}/>
    </React.StrictMode>
    ,
    document.getElementById("app")
);

