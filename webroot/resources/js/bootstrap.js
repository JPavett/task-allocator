import Swal from 'sweetalert2'
import toast from 'react-hot-toast'

/**
 * Load in packages to window object, so they don't need to be imported throughout
 * the application.
 */

window.Swal = Swal

window.toast = toast

window._ = require('lodash');
