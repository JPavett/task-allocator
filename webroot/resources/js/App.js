import React from "react";
import {Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import LogIn from "./pages/LogIn";
import RequireAuthentication from "./components/RequireAuthentication";
import TeamManagement from "./pages/TeamManagement";
import TaskManagement from "./pages/TaskManagement";
import Register from "./pages/Register";
import EditUser from "./pages/EditUser";
import EditTask from "./pages/EditTask";
import EditTeam from "./pages/EditTeam";
import AddTeam from "./pages/AddTeam";
import AddTask from "./pages/AddTask";

export default function App() {
    return (
        <Routes>
            <Route path="/" element={<RequireAuthentication> <Home/></RequireAuthentication>}/>
            <Route path="/teams" element={<RequireAuthentication> <TeamManagement/></RequireAuthentication>}/>
            <Route path="/tasks/team/" element={<RequireAuthentication> <TaskManagement/></RequireAuthentication>}/>
            <Route path="/tasks/team/:teamId" element={<RequireAuthentication> <TaskManagement/></RequireAuthentication>}/>
            <Route path="/users/:id" element={<RequireAuthentication> <EditUser/></RequireAuthentication>}/>
            <Route path="/tasks/add/team/:teamId" element={<RequireAuthentication> <AddTask/></RequireAuthentication>}/>
            <Route path="/tasks/:id" element={<RequireAuthentication> <EditTask/></RequireAuthentication>}/>
            <Route path="/teams/add" element={<RequireAuthentication> <AddTeam/></RequireAuthentication>}/>
            <Route path="/teams/:id" element={<RequireAuthentication> <EditTeam/></RequireAuthentication>}/>
            <Route path="/login" element={<LogIn/>}/>
            <Route path="/register" element={<Register/>}/>
        </Routes>
    );
}
