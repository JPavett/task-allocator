import React, {useEffect, useState} from 'react'
import Layout from '../components/Layout'
import {useNavigate, useParams} from "react-router";
import api from "../utilities/api";
import LoadingSpinner from "../components/LoadingSpinner";

const EditUser = () => {

    const [formInput, setFormInput] = useState({name: '', email: '', password: '', password_confirmation: ''})
    const [user, setUser] = useState([])
    const [loading, setLoading] = useState(true)
    const params = useParams();
    const navigate = useNavigate()

    const updateFormInput = e => {
        e.persist()

        setFormInput(prevState => ({...prevState, [e.target.name.slice(5)]: e.target.value}))
    }

    useEffect(() => {
        api().get('/api/users/' + params.id).then(response => {
            let responseUser = response.data.data
            setUser(responseUser)
            setFormInput({name: responseUser.name, email: responseUser.email, password: '', password_confirmation: ''})
            setLoading(false)
        })
    }, [])

    const deleteItem = e => {
        e.preventDefault()

        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'This will delete the account of ' + user.name + '. All tasks against this user will be set to Unallocated.',
            showCloseButton: true,
            showCancelButton: true,
        }).then(response => {
            if (response.isConfirmed) {
                api().delete('/api/users/' + user.id).then(response => {
                    toast.success(response.data.message)
                    navigate(-1)
                })
            }
        })
    }

    const saveItem = e => {
        e.preventDefault()

        api().put('/api/users/' + user.id, formInput).then(response => {
            setUser(response.data.data)
            toast.success(response.data.message)
        })
    }

    const goBack = e => {
        e.preventDefault()

        navigate(-1)
    }

    return (
        <Layout title="Edit User">
            <LoadingSpinner loading={loading}>
                <h3 className="font-medium text-gray-700">Editing User {user?.id}: {user?.name}</h3>
                <form className="mt-2 w-1/4" action="#" method="POST" autoComplete="off">
                    <div className="overflow-hidden sm:rounded-md">
                        <div className="mx-0 px-0 py-5">
                            <div className="grid grid-cols-6 gap-6">
                                <div className="col-span-6">
                                    <label htmlFor="name" className="block text-sm font-medium mb-2 text-gray-700">
                                        Name
                                    </label>
                                    <input type="text"
                                           onChange={updateFormInput}
                                           value={formInput.name}
                                           name="user_name"
                                           id="name"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6">
                                    <label htmlFor="email" className="block text-sm font-medium mb-2 text-gray-700">
                                        Email address
                                    </label>
                                    <input type="text"
                                           onChange={updateFormInput}
                                           value={formInput.email}
                                           autoComplete="new-email"
                                           name="user_email"
                                           id="user_email"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6">
                                    <label htmlFor="password" className="block text-sm font-medium mb-2 text-gray-700">
                                        Password
                                    </label>
                                    <input type="password"
                                           onChange={updateFormInput}
                                           value={formInput.password}
                                           name="user_password"
                                           autoComplete="new-password"
                                           id="password"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6">
                                    <label htmlFor="password_confirmation"
                                           className="block text-sm font-medium mb-2 text-gray-700">
                                        Confirm Password
                                    </label>
                                    <input type="password"
                                           onChange={updateFormInput}
                                           value={formInput.password_confirmation}
                                           name="user_password_confirmation"
                                           autoComplete="new-password"
                                           id="password_confirmation"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>
                            </div>
                        </div>
                        <div className="pl-4 py-3 text-right">
                            <button type="submit"
                                    onClick={goBack}
                                    className="inline-flex justify-center py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-sky-600 hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500">
                                Back
                            </button>
                            <button type="submit"
                                    onClick={deleteItem}
                                    className="inline-flex justify-center py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-rose-600 hover:bg-rose-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-rose-500">
                                Delete
                            </button>
                            <button type="submit"
                                    onClick={saveItem}
                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-600 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </LoadingSpinner>
        </Layout>
    )
}

export default EditUser
