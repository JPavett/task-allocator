import React, {useEffect, useState} from 'react'
import Layout from '../components/Layout'
import {useNavigate, useParams} from "react-router";
import api from "../utilities/api";
import LoadingSpinner from "../components/LoadingSpinner";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const EditTask = () => {

    const [formInput, setFormInput] = useState({title: '', description: '', priority: ''})
    const [task, setTask] = useState({users: []})
    const [loading, setLoading] = useState(true)
    const params = useParams();
    const navigate = useNavigate()

    const updateFormInput = e => {
        e.persist()

        setFormInput(prevState => ({...prevState, [e.target.name]: e.target.value}))
    }

    useEffect(() => {
        api().get('/api/tasks/' + params.id).then(response => {
            let task = response.data.data

            setTask(task)
            setFormInput({title: task.title, description: task.description, priority: task.priority})

            setLoading(false)
        })
    }, [])

    const deleteItem = e => {
        e.preventDefault()

        Swal.fire({
            icon: 'warning',
            title: 'Are you sure?',
            text: 'This will delete the task: ' + task.title + '.',
            showCloseButton: true,
            showCancelButton: true,
        }).then(response => {
            if (response.isConfirmed) {
                api().delete('/api/tasks/' + task.id).then(response => {
                    toast.success(response.data.message)
                    navigate(-1)
                })
            }
        })
    }

    const goBack = e => {
        e.preventDefault()

        navigate(-1)
    }

    const removeUser = (user) => {
        let userIndex = task.users.findIndex(taskUser => taskUser.id === user.id)
        task.users.splice(userIndex, 1)
        setTask(Object.assign({}, task))
    }

    const saveItem = e => {
        e.preventDefault()

        let userIds = [...task.users.map(x => x.id)]
        formInput.user_ids = [...new Set(userIds)]

        api().put('/api/tasks/' + task.id, formInput).then(response => {
            setTask(response.data.data)
            toast.success(response.data.message)
        })
    }

    return (
        <Layout title="Edit Task">
            <LoadingSpinner loading={loading}>
                <h3 className="font-medium text-gray-700">Editing Task {task?.id}: {task?.title}</h3>
                <form className="mt-2 w-1/2" action="#" method="POST" autoComplete="off">
                    <div className="overflow-hidden sm:rounded-md">
                        <div className="mx-0 px-0 py-5">
                            <div className="grid grid-cols-6 gap-6">
                                <div className="col-span-6">
                                    <label htmlFor="name" className="block text-sm font-medium mb-2 text-gray-700">
                                        Title
                                    </label>
                                    <input type="text"
                                           onChange={updateFormInput}
                                           value={formInput.title}
                                           name="title"
                                           id="title"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6">
                                    <label htmlFor="description"
                                           className="block text-sm font-medium mb-2 text-gray-700">
                                        Description
                                    </label>
                                    <textarea type="text"
                                              onChange={updateFormInput}
                                              value={formInput.description}
                                              name="description"
                                              id="description"
                                              className="h-28 px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6">
                                    <label htmlFor="description"
                                           className="block text-sm font-medium mb-2 text-gray-700">
                                        Assigned Users
                                    </label>
                                    {task.users.map(user =>
                                        <span key={user.id}>{user.name} <FontAwesomeIcon onClick={() => { removeUser(user)}} title="Remove User" className="ml-2" icon={'times'}/><br/></span>
                                    )}
                               </div>

                                <div className="col-span-1">
                                    <label htmlFor="department"
                                           className="block text-sm font-medium mb-2 text-gray-700">
                                        Priority
                                    </label>
                                    <select id="priority"
                                            name="priority"
                                            onChange={updateFormInput}
                                            value={formInput.priority}
                                            className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="pl-4 py-3 text-right">
                            <button type="submit"
                                    onClick={goBack}
                                    className="inline-flex justify-center py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-sky-600 hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-sky-500">
                                Back
                            </button>
                            <button type="submit"
                                    onClick={deleteItem}
                                    className="inline-flex justify-center py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-rose-600 hover:bg-rose-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-rose-500">
                                Delete
                            </button>
                            <button type="submit"
                                    onClick={saveItem}
                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-green-500 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </LoadingSpinner>
        </Layout>
    )
}

export default EditTask
