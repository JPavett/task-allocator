import React, {useEffect, useState} from 'react'
import Layout from '../components/Layout'
import api from "../utilities/api";
import LoadingSpinner from "../components/LoadingSpinner";
import TaskCard from "../components/TaskCard";

const Home = () => {

    const [user, setUser] = useState({ tasks: []})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        api().get('/user').then(response => {
            let user = response.data
            setUser({...user})
            setLoading(false)
        })
    }, [])

    const title = () => {
        if (user.team?.id) {
            return (
                <span className="text-lg font-medium">
                    You are in the {user.team.name}
                </span>
            )
        } else {
            return (
                <span className="text-md font-medium">
                    You are not allocated to a team, please speak to your Line Manager or a Site Admin
                </span>
            )
        }
    }

    const taskCount = () => {
        if (user.team?.id) {
            return (
                <span className="text-lg font-medium text-rose-600">
                    You have {user?.tasks?.length} Tasks allocated against you
                </span>
            )
        } else {
            return (
                <span className="text-md font-medium">
                    You are not allocated to a team, please speak to your Line Manager or a Site Admin
                </span>
            )
        }
    }

    const tasks = () => {
        if(user?.tasks?.length > 0) {
            return (
                <div className="flex flex-wrap">
                    {user.tasks.map(task =>
                        <div key={task.id} className="w-1/2 min-w-1/2 my-4">
                            <TaskCard task={task}/>
                        </div>
                    )}
                </div>
            )
        }
    }

    return (
        <Layout title="My Tasks">
            <LoadingSpinner loading={loading}>
                <div className="container">
                    {title()}<br/>
                    {taskCount()}<br/>
                    {tasks()}
                </div>
            </LoadingSpinner>
        </Layout>
    )
}

export default Home
