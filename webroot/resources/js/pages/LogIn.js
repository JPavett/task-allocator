import React, {useState} from 'react'
import api from '../utilities/api'
import User from "../utilities/user";
import useAuth from "../utilities/useAuth";
import {useLocation, useNavigate} from "react-router";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const LogIn = () => {
    const [formInput, setFormInput] = useState({email: '', password: ''})

    const {login} = useAuth();
    const navigate = useNavigate();
    const {state} = useLocation();

    const updateFormInput = e => {
        e.persist()

        setFormInput(prevState => ({...prevState, [e.target.name]: e.target.value}))
    }

    const signIn = e => {
        e.preventDefault()

        // Get cookie to authenticate against API
        api().get('/sanctum/csrf-cookie').then(() => {
            
            // If successful, attempt to log in using credentials
            api().post('/login', formInput).then(response => {
                User.authenticated(response.data.data)
                login().then(() => {
                    toast.success(response.data.message);
                    navigate(state?.path || "/");
                });
            })
        })
    }

    const goToRegisterPage = e => {
        e.preventDefault()

        navigate("/register");
    }

    // noinspection HtmlUnknownTarget
    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full">
                <div>
                    <img className="mx-auto w-auto" style={{width: 150}} src="/storage/images/logo.svg" alt="Workflow"/>
                    <h1 className="mt-6 text-center text-3xl font-bold text-gray-900">
                        Task Allocation Tool
                    </h1>
                    <h2 className="mt-6 text-center text-3xl font-bold text-gray-900">
                        Please Sign In or Register
                    </h2>
                </div>
                <form className="mt-8" action="#" method="POST">
                    <div className="rounded-md shadow-sm">
                        <div>
                            <input aria-label="Email address"
                                   name="email"
                                   type="email"
                                   required
                                   onChange={updateFormInput}
                                   className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:shadow-outline-rose focus:border-rose-300 focus:z-10 sm:text-sm sm:leading-5"
                                   placeholder="Email address"/>
                        </div>
                        <div className="-mt-px">
                            <input aria-label="Password"
                                   name="password"
                                   type="password"
                                   required
                                   onChange={updateFormInput}
                                   className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:shadow-outline-rose focus:border-rose-300 focus:z-10 sm:text-sm sm:leading-5"
                                   placeholder="Password"/>
                        </div>
                    </div>

                    <div className="mt-6 flex justify-center">
                        <button type="submit"
                                onClick={signIn}
                                className="group relative inline flex justify-center py-2 w-48 mx-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-rose-500 hover:bg-rose-500 focus:outline-none focus:border-rose-700 focus:shadow-outline-rose active:bg-rose-700 transition duration-150 ease-in-out">
                                  <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                                       <FontAwesomeIcon className="h-5 w-5 text-rose-300 transition ease-in-out duration-150"
                                                        icon={"lock"}/>
                                  </span>
                            Sign In
                        </button>
                        <button type="submit"
                                onClick={goToRegisterPage}
                                className="group relative inline flex justify-center py-2 w-48 mx-2 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-rose-500 hover:bg-rose-500 focus:outline-none focus:border-rose-700 focus:shadow-outline-rose active:bg-rose-700 transition duration-150 ease-in-out">
                                  <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                                      <FontAwesomeIcon className="h-5 w-5 text-rose-300 transition ease-in-out duration-150"
                                                       icon={"user"}/>
                                  </span>
                            Register
                        </button>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default LogIn
