import React, {useEffect, useState} from 'react'
import Layout from '../components/Layout'
import api from "../utilities/api";
import Card from "../components/Card";
import Draggable from "../components/Draggable";
import Droppable from "../components/Droppable";
import LoadingSpinner from "../components/LoadingSpinner";
import {useNavigate, useParams} from "react-router";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import User from "../utilities/user";
import TaskCard from "../components/TaskCard";

const TaskManagement = () => {
    const params = useParams();
    const [loading, setLoading] = useState(true)
    const [team, setTeam] = useState([])
    const [users, setUsers] = useState([])
    const [tasks, setTasks] = useState([])
    const navigate = useNavigate();

    useEffect(() => {
        getData()
    }, [])

    const getData = () => {
        if (params.teamId) {
            api().get('/api/teams/' + params.teamId).then(response => {
                setTeam(response.data.data)
                setTasks(response.data.data.tasks)
                setUsers(response.data.data.users)
                setLoading(false)

                if(!(params.teamId == User.team.id) && params.teamId && !User.isSuperAdmin()) {
                    toast.error('Invalid Request, You are not a member of this team. Navigating you back to your team')

                    setInterval(() => {
                        navigate('/tasks/team/' + User.team.id)
                    }, 2000)
                }

            })
        } else {
            setLoading(false)
        }
    }

    const updateTask = (task, user) => {
        api().put('/api/tasks/' + task.id, task).then(async response => {

            await getData()

            toast.success(user.name + ' assigned to Task ' + response.data.data.id)
        })
    }

    const goToAddTaskPage = () => {
        navigate('/tasks/add/team/' + User.team.id)
    }

    const goToEditUserPage = (user) => {
        navigate('/users/' + user.id)
    }

    const getPageTitle = () => {

        if(!params.teamId){
            return 'No Team Selected'
        } else if(team.id && !(params.teamId == team.id) && !User.isSuperAdmin()){
            return 'Invalid Team Selected'
        }

        return (team.name ?? "") + " Team Task Management"
    }

    const allocateUserToTask = (task, user) => {

        // Check if user is already allocated to task
        let alreadyExists = task.users.findIndex(taskUser => taskUser.id === user.id)

        if (alreadyExists !== -1) {
            toast.error(user.name + ' is already allocated to Task ' + task.id)
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Are you sure?',
                text: 'This will assign ' + user?.name + ' to Task ' + (task?.id),
                showCloseButton: true,
                showCancelButton: true,
            }).then(response => {
                if (response.isConfirmed) {
                    let userIds = [...task.users.map(x => x.id), user.id]
                    task.user_ids = [...new Set(userIds)]
                    updateTask(task, user)
                }
            })
        }
    }

    const pageDetail = () => {
        if(params.teamId && (params.teamId == User.team.id || User.isSuperAdmin())){

            return (
                <LoadingSpinner loading={loading}>
                    <div className="container flex">
                        <div className="w-1/3 pr-10">
                            <h3 className="text-xl font-bold leading-tight text-rose-500 pl-2">Users</h3>
                            {users.map(user =>
                                <Draggable key={user.id} itemData={user} fullHeight={false}>
                                    <Card title={user.name}
                                          subTitle={user.email}
                                          cardClass="h-24 mb-4"
                                          editable={true}
                                          draggable={true}
                                          allowEditByRegularUsers={true}
                                          onEdit={() => goToEditUserPage(user)}/>
                                </Draggable>
                            )}
                        </div>
                        <div className="w-2/3">
                            <h3 className="text-xl font-bold leading-tight block text-rose-500 pl-2">Tasks
                                <FontAwesomeIcon title="Add new Task"
                                                 style={{cursor: 'pointer'}}
                                                 className="ml-3"
                                                 onClick={goToAddTaskPage}
                                                 icon={'plus'}/>
                            </h3>
                            <div className="flex flex-wrap">
                                {tasks.map(task =>
                                    <div key={task.id} className="w-1/2 min-w-1/2 my-4">
                                        <Droppable dropData={task} onDrop={allocateUserToTask}>
                                            <TaskCard task={task}/>
                                        </Droppable>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </LoadingSpinner>
            )
        }
    }

    return (
        <Layout title={getPageTitle()}>
            {pageDetail()}
        </Layout>
    )
}

export default TaskManagement
