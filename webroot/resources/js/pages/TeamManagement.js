import React, {useEffect, useState} from 'react'
import Layout from '../components/Layout'
import api from "../utilities/api";
import Card from "../components/Card";
import Draggable from "../components/Draggable";
import Droppable from "../components/Droppable";
import LoadingSpinner from "../components/LoadingSpinner";
import {useNavigate} from "react-router";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import User from "../utilities/user";

const TeamManagement = () => {

    const [loading, setLoading] = useState(true)
    const [teams, setTeams] = useState([])
    const [users, setUsers] = useState([])
    const [usersGroup, setUsersGroup] = useState([])
    const [selectedTeam, setSelectedTeam] = useState('Unallocated')
    const navigate = useNavigate();

    useEffect(() => {
        getTeams(teams)
    }, [])

    useEffect(() => {
        getUsers()
    }, [])

    useEffect(() => {
        setUsersGroup(users.filter(user => {
            if (selectedTeam === 'Unallocated') {
                return !user?.team_id
            }
            return user?.team_id === selectedTeam.id
        }))
    }, [selectedTeam, users, teams])

    const getTeams = () => {
        api().get('/api/teams').then(response => {
            setTeams(JSON.parse(JSON.stringify(response.data.data)))
        })
    }

    const getUsers = () => {
        api().get('/api/users').then(response => {
            setUsers([...response.data.data])
            setLoading(false)
        })
    }

    const selectTeam = (team) => {
        setSelectedTeam(team)
    }

    const updateUser = (user) => {
        api().put('/api/users/' + user.id, user).then(response => {

            let responseUser = response.data.data

            getTeams()
            getUsers()

            if(user.id === User.id){
                api().get('/user').then(response => {
                    User.authenticated(responseUser)
                })
            }

            toast.success(responseUser.name + ' moved to ' + responseUser.team.name)
        })
    }

    const navigateToAddTeamPage = () => {
        navigate('/teams/add')
    }

    const navigateToEditTeamPage = (team) => {
        navigate('/teams/' + team.id)
    }

    const navigateToEditUserPage = (user) => {
        navigate('/users/' + user.id)
    }

    const addUserToTeam = (team, user) => {

        // Check if user already exists within team
        let updatedTeam = teams.find(t => t.id === team.id)
        let alreadyExists = updatedTeam?.users?.findIndex(teamUser => teamUser?.id === user.id)

        if (alreadyExists >= 0 || (team === 'Unallocated' && user.team_id === null)) {
            toast.error(user.name + ' is already allocated to ' + (team?.name || team))
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'Are you sure?',
                text: 'This will move ' + user?.name + ' to ' + (team?.name || team),
                showCloseButton: true,
                showCancelButton: true,
            }).then(response => {
                if (response.isConfirmed) {
                    if (team === 'Unallocated') {
                        user.team_id = null
                    } else {
                        user.team_id = team.id
                    }
                    updateUser(user)
                }
            })
        }

    }

    return (
        <Layout title="Team Management">
            <LoadingSpinner loading={loading}>
                <div className="container flex">
                    <div className="w-1/3 pr-10">
                        <h3 className="text-xl font-bold leading-tight text-rose-500 pl-2">Teams
                            <FontAwesomeIcon title="Add new Team"
                                             style={{cursor: 'pointer'}}
                                             className="ml-3"
                                             onClick={navigateToAddTeamPage}
                                             icon={'plus'}/>
                        </h3>
                        <Droppable key="0" dropData="Unallocated" fullHeight={false} onDrop={addUserToTeam}>
                            <Card onClick={() => selectTeam('Unallocated')}
                                  title="Unallocated"
                                  selected={selectedTeam === 'Unallocated'}
                                  subTitle={users.filter(user => user.team_id === null).length + ' Members'}
                                  description="Users with no team"
                                  cardClass="h-24 mb-4"/>
                        </Droppable>
                        {teams.map((team, index) =>
                            <Droppable key={team.id} dropData={team} fullHeight={false} onDrop={addUserToTeam}>
                                <Card onClick={() => selectTeam(team)}
                                      title={team.name}
                                      selected={team.id === selectedTeam.id}
                                      subTitle={team.users.length + ' Members'}
                                      description={team.description}
                                      cardClass="h-24 mb-4"
                                      editable={true}
                                      onEdit={() => navigateToEditTeamPage(team)}/>
                            </Droppable>
                        )}
                    </div>
                    <div className="w-2/3">
                        <h3 className="text-xl font-bold leading-tight block text-rose-500 pl-2">{selectedTeam.name || selectedTeam} Users</h3>
                        <div className="flex flex-wrap">
                            {usersGroup.map((user, index) =>
                                <div key={user.id} className="w-1/2 min-w-1/2 my-2">
                                    <Draggable itemData={user}>
                                        <Card title={user.name}
                                              description={user.email}
                                              editable={true}
                                              draggable={true}
                                              onEdit={() => navigateToEditUserPage(user)}/>
                                    </Draggable>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </LoadingSpinner>
        </Layout>
    )
}

export default TeamManagement
