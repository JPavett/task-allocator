import React, {useState} from 'react'
import api from '../utilities/api'
import {useNavigate} from "react-router";

const Register = () => {
    const [formInput, setFormInput] = useState({name: '', email: '', password: '', password_confirmation: ''})

    const navigate = useNavigate();

    const updateFormInput = e => {
        e.persist()

        setFormInput(prevState => ({...prevState, [e.target.name]: e.target.value}))
    }

    const register = e => {
        e.preventDefault()

        api().post('/register', formInput).then(response => {
            toast.success(response.data.message);
            navigate("/login");
        })
    }

    const goToLogInPage = e => {
        e.preventDefault()

        navigate("/login");
    }

    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
            <div className="w-1/3">
                <div>
                    <img className="mx-auto w-auto"
                         style={{width: 150}} 
                         src="/storage/images/logo.svg"
                         alt="Workflow"/>
                    <h2 className="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
                        Please enter your details below </h2>
                </div>
                <form className="mt-8" action="#" method="POST">
                    <div className="overflow-hidden sm:rounded-md">
                        <div className="px-4 py-5 sm:p-6">
                            <div className="grid grid-cols-6 gap-6">
                                <div className="col-span-6 sm:col-span-4">
                                    <label htmlFor="name" className="block text-sm font-medium mb-2 text-gray-700">
                                        Name
                                    </label>
                                    <input type="text"
                                           onChange={updateFormInput}
                                           name="name"
                                           id="name"
                                           autoComplete="given-name"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6 sm:col-span-4">
                                    <label htmlFor="email" className="block text-sm font-medium mb-2 text-gray-700">
                                        Email address
                                    </label>
                                    <input type="text"
                                           onChange={updateFormInput}
                                           name="email"
                                           id="email"
                                           autoComplete="email"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6 sm:col-span-3">
                                    <label htmlFor="password" className="block text-sm font-medium mb-2 text-gray-700">
                                        Password
                                    </label>
                                    <input type="password"
                                           onChange={updateFormInput}
                                           name="password"
                                           id="password"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>

                                <div className="col-span-6 sm:col-span-3">
                                    <label htmlFor="password_confirmation"
                                           className="block text-sm font-medium mb-2 text-gray-700">
                                        Confirm Password
                                    </label>
                                    <input type="password"
                                           onChange={updateFormInput}
                                           name="password_confirmation"
                                           id="password_confirmation"
                                           className="px-3 py-2 focus:outline-none focus:ring-rose-500 focus:border-rose-500 block w-full shadow-sm sm:text-sm border border-gray-300 rounded-md"/>
                                </div>
                            </div>
                        </div>
                        <div className="px-4 py-3 text-right sm:px-6">
                            <button type="submit"
                                    onClick={goToLogInPage}
                                    className="inline-flex justify-center py-2 px-4 mr-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-rose-600 hover:bg-rose-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-rose-500">
                                Login
                            </button>
                            <button type="submit"
                                    onClick={register}
                                    className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-rose-600 hover:bg-rose-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-rose-500">
                                Register
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Register
