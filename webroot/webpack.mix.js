const mix = require('laravel-mix');

require('mix-tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.webpackConfig({
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        client: {
            logging: 'verbose',
            overlay: true,
        }
    },
});

mix.options({
    hmrOptions: {
        host: 'localhost',
        port: 8007,
    },
});

mix.js('resources/js/index.js', 'public/js/app.js')
    .extract()
    .react()
    .sass('resources/sass/app.scss', 'public/css')
    .tailwind()
    .version();
