<?php

use App\Http\Controllers\AuthenticationController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    /** @var User $user */
    $user = $request->user();

    return $user->load(['team', 'tasks'])->toArray();
});

Route::fallback(function () {
    return view('landing');
})->name('landing');

Route::post('/login', [AuthenticationController::class, 'logIn'])->name('login');
Route::post('/logout', [AuthenticationController::class, 'logOut'])->name('logout');
Route::post('/register', [AuthenticationController::class, 'register'])->name('register');
